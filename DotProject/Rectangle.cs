﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp15
{
    class Rectangle : Dot
    {
        public int length;
        public int width;

        public override void Draw()
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    base.Draw();
                }
                Console.WriteLine();
            }
        }


    }
}
