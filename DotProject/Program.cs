﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("1) Dot: ");
            Dot D = new Dot();
            D.Draw();
            Console.WriteLine();
            Console.WriteLine();

            Console.Write("2) Line: ");
            Line Ln = new Line { length = 10 };
            Ln.Draw();
            Console.WriteLine();
            Console.WriteLine();

            Console.Write("3) Arrow: ");
            Arrow Arr = new Arrow { length = 15 };
            Arr.Draw();
            Console.WriteLine();
            Console.WriteLine();

            Console.Write("4) Rectangle: ");
            Console.WriteLine();
            Rectangle Rec = new Rectangle { length = 8, width = 12 };
            Rec.Draw();
            Console.WriteLine();
            Console.WriteLine();

            Console.Write("5) Square: ");
            Console.WriteLine();
            Square Sq = new Square { length = 8, width = 8 };
            Sq.Draw();
            Console.WriteLine();
            Console.WriteLine();

            Console.Write("6) Triangle: ");
            Console.WriteLine();
            Triangle Trg = new Triangle { x = 0, y = 15 };
            Trg.DrawTriangle();
            Console.WriteLine();
            Console.WriteLine();

        }
    }
}
