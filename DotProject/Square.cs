﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp15
{
    class Square : Rectangle
    {

        public override void Draw()
        {
            if (length != width)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("It Is Not A Square");

            }
            base.Draw();
        }

    }
}
