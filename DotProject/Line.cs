﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp15
{
    class Line : Dot
    {

        public int length;
        public override void Draw()
        {
            for (int i = 0; i < length; i++)
            {
                base.Draw();
            }
        }
    }
}
